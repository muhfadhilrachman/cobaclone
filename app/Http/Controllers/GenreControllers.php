<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GenreControllers extends Controller
{
    public function get()
    {
        // return view('listBuku');
        $genre = DB::table('table_genre')->get();
        // dd($genre);
        return view('genre.listGenre',['data'=>$genre]);

    }
    
    public function create(Request $request)
    {

        $request->validate([
            'nama'=> 'required',
            'deskripsi'=> 'required',
        ]);
        DB::table('table_genre')->insert(
            [
                'nama'=>$request['nama'],
                'deskripsi'=>$request['deskripsi'],
            ]
            );
        return redirect('/genre');
    }

    public function update($id, Request $request)
    {
        DB::table('table_genre')
        ->where('id',$id)
        ->update(
            [
                'nama'=>$request['nama'],
                'deskripsi'=>$request['deskripsi'],
            ]
            );
        return redirect('/genre');

    }
    public function delete($id)
    {
        DB::table('table_genre')
        ->where('id',$id)
        ->delete();
        
        return redirect('/genre');
        
    }
}
