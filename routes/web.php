<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GenreControllers;
use App\Http\Controllers\penerbitController;
use App\Http\Controllers\BukuController;
use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('buku/listBUku');
});

// ROUTING BUKU
Route::get('/buku',[BukuController::class, 'get']);

// Route::get


// ROUTING GENRE

Route::get('/genre',[GenreControllers::class, 'get']);

Route::post('/genre',[GenreControllers::class, 'create']);

Route::get('/genre/create',function(){
    return view('genre.createGenre');
});

Route::get('genre/{genre_id}/update',function($id){
    $genre= DB::table('table_genre')->find($id);
    return view('genre.updateGenre',['genre'=>$genre]);
});

Route::put('genre/{genre_id}/update',[GenreControllers::class, 'update']);

Route::delete('genre/{genre_id}',[GenreControllers::class, 'delete']);



// ROUTING PENERBIT

//get
Route::get('/penerbit',[penerbitController::class, 'get']);

//post
Route::post('/penerbit',[penerbitController::class, 'create']);

Route::get('/penerbit/create',function(){
    return view('penerbit.createPenerbit');
});

//put
Route::get('penerbit/{id}/update',function($id){
    $data= DB::table('table_penerbit')->find($id);
    return view('penerbit.updatePenerbit',['data'=>$data]);
});

Route::put('penerbit/{genre_id}/update',[penerbitController::class, 'update']);

//delete

Route::delete('penerbit/{genre_id}',[penerbitController::class, 'delete']);

