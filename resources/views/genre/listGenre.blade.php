@extends('template')

@section('title')
Genre
@endsection
@section('btn')
<a class="btn btn-primary" href='/genre/create'>+</a>
@endsection
@section('content')
<table class="table">
    <thead>
      <tr>
        <th>NO</th>
        <th>Nama</th>
        <th>Deskripsi</th>
        <th>Tindakan</th>
      </tr>
    </thead>
<tbody>
    @forelse ($data as $key => $item)
        <tr>
            <td>{{$key +1}}</td>
            <td>{{$item ->nama}}</td>
            <td class="text-align">{{$item ->deskripsi}}</td>
            <td>
                <form action="/genre/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="genre/{{$item->id}}/update" class="btn btn-success">Update</a>
                    <input  value="delete" type="submit" class="btn btn-primary">
                </form>
            </td>
        </tr>
    @empty
    <tr>
        <td colspan="4" class="text-center">tidak ada data</td>
    </tr>
    @endforelse
</tbody>
  </table>
@endsection
