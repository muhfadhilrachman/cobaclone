@extends('template');

@section('title')
Create Genre
@endsection

@section('content')
<form action="/penerbit" method="POST">
    @csrf
    <div class="form-group">
      <label for="exampleInputEmail1">Nama Penerbit</label>
      <input  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="...." name="nama">
      @error('nama')
          <div class="alert alert-danger mt-1">{{$message}}</div>
      @enderror
      {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Alamat Penerbit</label>
        <input  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="...." name="alamat">
        @error('alamat')
            <div class="alert alert-danger mt-1">{{$message}}</div>
        @enderror
        {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
      </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Deskripsi</label>
      <textarea  class="form-control" name="deskripsi" ></textarea>
      @error('deskripsi')
      <div class="alert alert-danger mt-1">{{$message}}</div>
  @enderror
    </div>
  
    <button type="submit" class="btn btn-success">Submit</button>
            <a  class="btn btn-primary" href="/genre">
                Back
            </a>
  </form>
@endsection